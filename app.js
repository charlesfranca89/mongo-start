const express = require('express');
var bodyParser = require('body-parser')
const app = express();
const port = 3000;
const { MongoClient } = require("mongodb");
const uri = "mongodb://localhost";
const client = new MongoClient(uri);
let database;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', (req, res) => {
    const collection = database.collection('sites');
    let query = {};
    if (req.query.name) {
        query["name"] = req.query.name;
    }

    if (req.query.price) {
        query = {
            ...query,
            price: { $lte: parseInt(req.query.price) }
        }
    }

    collection.find(query).toArray(function (error, documents) {
        if (error) throw error;

        res.json(documents);
    });
});

app.post('/', async (req, res) => {
    const collection = database.collection('sites');
    const site = {
        name: "Neapolitan pizza"
    };
    const result = await collection.insertOne(site);
    res.json({
        status: true,
        result
    });
});

async function run() {
    try {
        await client.connect().then(() => {
            database = client.db('mydb');

            // Iniciar a api
            app.listen(port, () => {
                console.log('conectado')
                console.log(`Example app listening at http://localhost:${port}`)
            });
        }).catch(error => {
            console.log(error);
        });
    } finally {
    }
}

run();